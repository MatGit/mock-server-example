package com.cdiscount.example;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jsoup.Jsoup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class GetImagesService {

    private String url;

    public GetImagesService(String url) {
        this.url = url;
    }

    public List<String> getUrls() throws IOException {
        try (CloseableHttpClient client = HttpClientBuilder.create().build()) {
            HttpGet request = new HttpGet(url);

            completeHeader(request);

            CloseableHttpResponse response = client.execute(request);

            System.out.println("Response status : " + response.getStatusLine().getStatusCode());

            String content = getResponseContent(response);

            return parseImagesUrls(content);
        }
    }

    private List<String> parseImagesUrls(String content) {
        List<String> urls = new ArrayList<>();
        Jsoup.parse(content)
                .body()
                .getElementsByTag("img")
                .forEach(image -> urls.add(image.attr("src")));

        return urls;
    }

    private String getResponseContent(CloseableHttpResponse response) throws IOException {
        try (BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()))) {

            StringBuilder result = new StringBuilder();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            return result.toString();
        }
    }

    private void completeHeader(HttpGet request) {
        request.addHeader("authority", "www.amazon.fr");
        request.addHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/71.0.3578.98 Chrome/71.0.3578.98 Safari/537.36");
        request.addHeader("Upgrade-Insecure-Requests", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        request.addHeader("accept", "1");
        request.addHeader("accept-encoding", "gzip, deflate, br");
        request.addHeader("accept-language", "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7,de;q=0.6,es;q=0.5,ja;q=0.4,zh-CN;q=0.3,zh;q=0.2,ko;q=0.1");
    }
}
