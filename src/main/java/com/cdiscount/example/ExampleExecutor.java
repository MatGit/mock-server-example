package com.cdiscount.example;

import java.io.IOException;
import java.util.List;

public class ExampleExecutor {

    public static void main(String[] args) {

        try {
            List<String> urls = new GetImagesService("https://www.amazon.fr").getUrls();

            urls.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
