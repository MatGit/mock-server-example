package com.cdiscount.example;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class GetImagesServiceIntegrationTest {

    @Test
    public void testGetUrls() throws IOException {
        List<String> urls = new GetImagesService("http://127.0.0.1:1080/images").getUrls();

        assertThat(urls.size()).isGreaterThan(0);
    }
}
