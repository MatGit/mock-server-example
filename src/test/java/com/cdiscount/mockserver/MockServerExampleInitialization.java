package com.cdiscount.mockserver;

import org.mockserver.client.MockServerClient;
import org.mockserver.client.initialize.ExpectationInitializer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public class MockServerExampleInitialization implements ExpectationInitializer {

    @Override
    public void initializeExpectations(MockServerClient mockServerClient) {
        try (InputStream stream = MockServerExampleInitialization.class.getResourceAsStream("/amazon.html");
             BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
            StringBuilder content = new StringBuilder();
            String line = null;
            while( (line = reader.readLine()) != null ) {
                content.append(line);
            }

            mockServerClient.when(request().withPath("/images"))
                    .respond(response().withStatusCode(200)
                            .withBody(content.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
